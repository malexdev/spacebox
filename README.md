# spacebox
Code editor built for Meteor with Meteor, with source control built in.

# Demo
The spacebox demo is hosted courtesy of the Meteor group at [spacebox.meteor.com](http://spacebox.meteor.com)! 

# Goals
Eventually, spacebox will be an editor that: 
- Runs in the browser on a server.
- Supports web, node/io.js, and meteor projects.
- Manages running the project for the user.
- Truly understands how the project types it supports work, including type inference and variable tracking.
- Manages files in the local meteor database rather than on the file system (can export files for download).
- Manages files on a per-user basis, while also allowing users to share files to other users (for view only or edit).
- Allows users to collaboratively edit files and chat.
- Manages source control with versioning for files. Supports exporting source control (including history) to git, and importing from git.
