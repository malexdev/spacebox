Template.modalNewFile.events({
    'click #buttonNewFileCreate': function() {
        // make sure we have the file name
        var fileName = $('#inputNewFileName').val();
        if (!fileName) {
            return Session.setTemp(constants.sessionKeys.newFile.error, constants.error.newFile.noFileName);
        }

        // determine if we should create the file as a child or sibling of our current context menu target
        var contextTarget = Session.get(constants.sessionKeys.current.contextMenuTarget);
        var createAsChild = contextTarget.folder || isProjectObject(contextTarget);

        // create the file in the database
        Files.insert({
            name: fileName,
            parent: createAsChild ? contextTarget._id : contextTarget.parent,
            folder: false,
            content: ''
        });

        Session.clear(constants.sessionKeys.newFile.error);
        $('#inputNewFileName').val('');
        modal.hide('NewFile');
    },
    'click #modalNewFile button': function(e) {
        if (e.target.id !== 'buttonNewFileCreate') {
            Session.clear(constants.sessionKeys.newFile.error);
            $('#inputNewFileName').val('');
        }
    },
});

Template.modalNewFile.helpers({
    newFileError: function() {
        return Session.get(constants.sessionKeys.newFile.error);
    }
});