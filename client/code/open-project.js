Template.modalOpenProject.helpers({
    projectList: function() {
        return Projects.find({owner: Meteor.userId()});
    },
    nameForProjectType: function() {
        return _.findWhere(constants.project.types, {value: this.type}).name;
    },
    selectedProject: function() {
        var selectedProject = Session.get(constants.sessionKeys.openProject.selectedToOpen);
        if (selectedProject) {
            return _.isEqual(selectedProject, this);
        } else {
            return false;
        }
    }
});

Template.modalOpenProject.events({
    'click #modalOpenProject .list-group-item': function(e) {
        Session.setTemp(constants.sessionKeys.openProject.selectedToOpen, this);
    },
    'click #modalOpenProject button': function(e) {
        if (e.target.id !== 'buttonOpenProject') {
            Session.clear(constants.sessionKeys.openProject.selectedToOpen);
        }
    },
    'click #buttonOpenProject': function() {
        var project = Session.get(constants.sessionKeys.openProject.selectedToOpen);
        if (project) {
            Session.set(constants.sessionKeys.current.project, project);
            Session.clear(constants.sessionKeys.openProject.selectedToOpen);
            modal.hide('OpenProject');
        }
    }
});