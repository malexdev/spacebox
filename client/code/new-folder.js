Template.modalNewFolder.events({
    'click #buttonNewFolderCreate': function() {
        // make sure we have the file name
        var folderName = $('#inputNewFolderName').val();
        if (!folderName) {
            return Session.setTemp(constants.sessionKeys.newFolder.error, constants.error.newFolder.noFolderName);
        }

        // determine if we should create the file as a child or sibling of our current context menu target
        var contextTarget = Session.get(constants.sessionKeys.current.contextMenuTarget);
        var createAsChild = contextTarget.folder || isProjectObject(contextTarget);

        // create the file in the database
        Files.insert({
            name: folderName,
            parent: createAsChild ? contextTarget._id : contextTarget.parent,
            folder: true,
            expanded: false
        });


        Session.clear(constants.sessionKeys.newFile.error);
        $('#inputNewFolderName').val('');
        modal.hide('NewFolder');
    },
    'click #modalNewFolder button': function(e) {
        if (e.target.id !== 'buttonNewFolderCreate') {
            Session.clear(constants.sessionKeys.newFile.error);
            $('#inputNewFolderName').val('');
        }
    },
});

Template.modalNewFolder.helpers({
    newFolderError: function() {
        return Session.get(constants.sessionKeys.newFolder.error);
    }
});