/*
 * This file contains various bits of code that need to auto-run when things change. 
 */

Meteor.startup(function() {
    Tracker.autorun(updateSelectedProject);
    Tracker.autorun(updateSelectedFile);
    Tracker.autorun(destroySessionOnLogout);
    Tracker.autorun(setSessionDefaultsOnLogin);
});

/**
 * Update the currently selected project object in the session when the object changes in the database.
 */
function updateSelectedProject() {
    var currentProject = Session.get(constants.sessionKeys.current.project);
    if (currentProject) {
        var dbProject = Projects.findOne({_id: currentProject._id});
        if (dbProject && !_.isEqual(currentProject, dbProject)) {
            Session.set(constants.sessionKeys.current.project, Projects.findOne({_id: currentProject._id}));
        }
    }
}

function updateSelectedFile() {
    var currentFile = Session.get(constants.sessionKeys.current.file);
    if (currentFile) {
        var dbFile = Files.findOne({_id: currentFile._id});
        if (dbFile && !_.isEqual(currentFile, dbFile)) {
            var content = dbFile.content ? dbFile.content.toString() : '';
            Session.set(constants.sessionKeys.current.file, dbFile);
            Session.set(constants.sessionKeys.fileEditorAreaContents, content);
        }
    }
}

function destroySessionOnLogout() {
    var loggedIn = Meteor.userId();
    if (!loggedIn) { Session.clear(); }
}

function setSessionDefaultsOnLogin() {
    var loggedIn = Meteor.userId();
    if (loggedIn) {
        _.keys(constants.sessionKeys.default).forEach(function(sessionKey) {
            Session.set(sessionKey, constants.sessionKeys.default[sessionKey]);
        });
    }
}