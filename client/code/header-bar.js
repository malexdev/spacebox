Template.headerBar.created = function() {
    
}

Template.headerBar.helpers({
    applicationName: function() {
        return constants.application.name;
    }
});

Template.headerBarFileDropdown.helpers({
    title: function() {
        return constants.application.menu.file.title;
    },
    openFile: function() {
        return constants.application.menu.file.openFile;
    },
    openProject: function() {
        return constants.application.menu.file.openProject;
    },
    saveFile: function() {
        return constants.application.menu.file.saveFile;
    },
    saveProject: function() {
        return constants.application.menu.file.saveProject;
    },
    newProject: function() {
        return constants.application.menu.file.newProject;
    },
    newFile: function() {
        return constants.application.menu.file.newFile;
    },
    newFolder: function() {
        return constants.application.menu.file.newFolder;
    }
});

Template.headerBarFileDropdown.events({
    'click #newProject': function() {
        $('#modalNewProject').modal('show');
    },
    'click #openProject': function() {
        if (Projects.find({owner: Meteor.userId()}).fetch().length == 0) {
            swal({
                title: 'No Projects',
                text: 'There are no projects to open.',
                type: 'error',
                confirmButtonClass: 'btn-danger'
            });
        } else {
            modal.show('OpenProject');
        }
    },
    'click #newFolder': function() {
        modal.show('NewFolder');
    },
    'click #newFile': function() {
        modal.show('NewFile');
    }
});

Template.headerBarEditDropdown.helpers({
    title: function() {
        return constants.application.menu.edit.title;
    },
    undo: function() {
        return constants.application.menu.edit.undo;
    },
    redo: function() {
        return constants.application.menu.edit.redo;
    },
    cut: function() {
        return constants.application.menu.edit.cut;
    },
    copy: function() {
        return constants.application.menu.edit.copy;
    },
    paste: function() {
        return constants.application.menu.edit.paste;
    },
    selectAll: function() {
        return constants.application.menu.edit.selectAll;
    }
});

Template.headerBarViewDropdown.helpers({
    title: function() {
        return constants.application.menu.view.title;
    },
    console: function() {
        return constants.application.menu.view.console;
    },
    editor: function() {
        return constants.application.menu.view.editor;
    },
    fileBrowser: function() {
        return constants.application.menu.view.fileBrowser;
    },
    fileHistory: function() {
        return constants.application.menu.view.fileHistory;
    }
});

Template.headerBarViewDropdown.events({
    'click li a': function(e) {
        toggleSessionOption(constants.sessionKeys.view[e.target.id]);
    }
});

Template.projectDropdown.helpers({
    shareProject: function() {
        return constants.application.menu.project.shareProject;
    },
    deleteProject: function() {
        return constants.application.menu.project.deleteProject;
    },
    closeProject: function() {
        return constants.application.menu.project.closeProject;
    },
    renameProject: function() {
        return constants.application.menu.project.renameProject;
    }
});

Template.projectDropdown.events({
    'click #renameProject': function() {
        modal.show('modalRenameProject');
    },
    'click #closeProject': function() {
        Session.set(constants.sessionKeys.current.project, null);
    },
    'click #deleteProject': function() {
        // get the project name
        var project = Session.get(constants.sessionKeys.current.project);
        swal({
            title: util.format('Delete {{name}}?', {name: project.name}),
            text: 'Project deletion is permanent!',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: util.format('I understand. Delete {{name}}.', {name: project.name}),
            closeOnConfirm: true
        }, function() {
            Session.set(constants.sessionKeys.current.project, null);
            Projects.remove({_id: project._id});
        });
    },
    'click #shareProject': function() {
        commonAlerts.notYetImplemented();
    }
});