Template.modalRenameProject.events({
    'click #buttonRenameProject': function() {
        var newProjectName = $('#inputRenameProjectName').val();
        var currentProject = Session.get(constants.sessionKeys.current.project);
        
        if (!newProjectName) {
            return Session.set(constants.sessionKeys.renameProject.error, constants.error.renameProject.noProjectName);
        } 
        if (!currentProject) {
            return Session.set(constants.sessionKeys.renameProject.error, constants.error.noProjectSelected);
        }
        
        Session.set(constants.sessionKeys.renameProject.error, null);
        Projects.update({_id: currentProject._id}, {$set: {name: newProjectName}});
        modal.hide('RenameProject');
    }
});

Template.modalRenameProject.helpers({
    renameProjectError: function() {
        return Session.get(constants.sessionKeys.renameProject.error);
    }
});