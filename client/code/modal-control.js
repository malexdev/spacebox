modal = {
    show: function(name) {
        name = generateSelectorForModal(name);
        $(name).modal('show');
    },
    hide: function(name) {
        name = generateSelectorForModal(name);
        $(name).modal('hide');
    }
}

function generateSelectorForModal(name) {
    if (!name.match(/^modal/)) { name = 'modal' + name; }
    if (!name.match(/^#/)) { name = '#' + name; }
    return name;
}