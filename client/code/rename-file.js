Template.modalRenameFile.events({
    'click #buttonRenameFile': function() {
        var newFileName = $('#inputRenameFileName').val();
        var currentFile = Session.get(constants.sessionKeys.current.file);

        if (!newFileName) {
            return Session.set(constants.sessionKeys.renameFile.error, constants.error.renameFile.noFileName);
        } 
        if (!currentFile) {
            return Session.set(constants.sessionKeys.renameFile.error, constants.error.noFileSelected);
        }

        Session.clear(constants.sessionKeys.renameFile.error);
        Files.update({_id: currentFile._id}, {$set: {name: newFileName}});
        modal.hide('RenameFile');
    }
});

Template.modalRenameFile.helpers({
    renameFileError: function() {
        return Session.get(constants.sessionKeys.renameFile.error);
    }
});