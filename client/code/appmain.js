UI.registerHelper('viewConsoleEnabled', function() {
    return Session.get(constants.sessionKeys.view.console);
});

UI.registerHelper('viewFileBrowserEnabled', function() {
    return Session.get(constants.sessionKeys.view.fileBrowser) && Session.get(constants.sessionKeys.current.project);
});

UI.registerHelper('viewEditorEnabled', function() {
    return Session.get(constants.sessionKeys.view.editor) && Session.get(constants.sessionKeys.current.project);
});

UI.registerHelper('viewFileHistoryEnabled', function() {
    return Session.get(constants.sessionKeys.view.fileHistory);
});

UI.registerHelper('currentProject', function() {
    return Session.get(constants.sessionKeys.current.project);
});

UI.registerHelper('currentFile', function() {
    return Session.get(constants.sessionKeys.current.file);
});

UI.registerHelper('currentFilePendingSave', function() {
    return Session.get(constants.sessionKeys.current.filePendingSave);
});

/**
 * This function re-runs each time the reactive data source changes, and runs the specified function after the DOM finishes updating.
 * @param {Object}   reactiveDataSource The data source to trigger re-running this function on. Can be a function (no args) or a reactive variable.
 * @param {Function} functionToRun      The function to run after the reactive data source changes and the DOM finishes updating.
 */
runAfterAllRendered = function(reactiveDataSource, functionToRun) {
    Tracker.autorun(function() {
        var trash = reactiveDataSource(); // we don't actually care about this variable, we just want it to trigger the autorun
        Deps.afterFlush(functionToRun);
    });
}

