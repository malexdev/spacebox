Template.fileTree.helpers({
    children: function() {
        var project = Session.get(constants.sessionKeys.current.project);
        if (project) {
            return getFileChildren(project, 0);
        } else {
            return null;
        }
    },
    isSelectedFile: function() {
        var selectedFile = Session.get(constants.sessionKeys.current.file);
        return this._id === selectedFile._id;
    },
    isFolderClosed: function() {
        return this.folder && !this.expanded;
    },
    isFolderExpanded: function() {
        return this.folder && this.expanded;
    },
    isFile: function() {
        return !this.folder;
    }
});

Template.fileTree.events({
    'click .list-group-item': function() {
        // if we're clicking the project group header, just return
        if (isProjectObject(this)) { return; }
        
        // if this is a folder, toggle the expanded state
        if (this.folder) {
            Files.update({_id: this._id}, {$set: {expanded: !this.expanded}});
        } else {
            // if this is a file, toggle the selected state
            if (_.isEqual(this, Session.get(constants.sessionKeys.current.file))) {
                Session.clear(constants.sessionKeys.current.file);
            } else {
                Session.setTemp(constants.sessionKeys.current.file, this);
            }
        }
    }
});

Template.fileTree.destroyed = function() {
    endResizing();
};

Template.fileTree.created = function() {
    startResizing();
};

Template.fileTree.rendered = function() {
    runAfterAllRendered(function() { return getFileChildren(Session.get(constants.sessionKeys.current.project), 0); }, bindFileTreeRightClick);
}

function startResizing() {
    $(window).resize(resize);
}

function endResizing() {
    $(window).off('resize', resize);
}

function resize() {
    $('#fileTree').height($(window).height() - constants.application.elementBottomOffset);
}

var bindFileTreeRightClick = _.debounce(function() {
    $('.tree-context').contextmenu({
        target:'#fileTreeContextMenu', 
        before: function(e, context) {
            e.preventDefault();

            // get the id of the file that was right clicked
            var id = e.target.id;

            // check if this is the project parent
            var isProjectMenuItem = isProjectObject({_id: id});

            // set this document to the context menu target
            var contextTarget;
            if (isProjectMenuItem) {
                contextTarget = Projects.findOne({_id: id});
            } else {
                contextTarget = Files.findOne({_id: id});
            }

            // log it
            Session.setTemp(constants.sessionKeys.current.contextMenuTarget, contextTarget);
            //console.log('Set context target to %s', JSON.stringify(contextTarget));

            // show the menu
            return true;
        }
    });
}, 100);

getFileChildren = function(parent, parentLevel) {
    if (!parent) { return null; } // if there's no parent to find the children of... not much we can do!
    
    var children = [];
    var thisLevel = parentLevel + 1;
    Files.find({parent: parent._id}).forEach(function(child) {
        // add this child to the array
        child.level = thisLevel;
        children.push(child);
        
        // recurse into the children of this child, if this is a folder and it is expanded
        if (child.folder && child.expanded) {
            var grandchildren = getFileChildren(child, thisLevel);
            grandchildren.forEach(function(grandchild) {
                children.push(grandchild);
            });
        }
    });
    return children;
}