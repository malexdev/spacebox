Template.modalNewProject.helpers({
    projectTypes: function() {
        return constants.project.types;
    },
    newProjectType: function() {
        var project = Session.get(constants.sessionKeys.newProject.type);
        return project ? project.name : null;
    },
    newProjectError: function() {
        return Session.get(constants.sessionKeys.newProject.error);
    }
});

Template.modalNewProject.events({
    'click #selectNewProjectType li a': function(e) {
        Session.set(constants.sessionKeys.newProject.type, this);
    },
    'click #buttonNewProjectCreate': function(e) {
        // check that we have all the items needed
        var name = $('#inputNewProjectName').val()
        var type = Session.get(constants.sessionKeys.newProject.type).value;
        
        if (!name) { return Session.set(constants.sessionKeys.newProject.error, constants.error.newProject.noProjectName); }
        if (!type) { return Session.set(constants.sessionKeys.newProject.error, constants.error.newProject.noProjectType); }
        Session.set(constants.sessionKeys.newProject.error, null);
        
        // create the project
        var newProjectId = Projects.insert({owner: Meteor.userId(), name: name, type: type});
        
        // set this project as the current project
        Session.set(constants.sessionKeys.current.project, Projects.findOne({_id: newProjectId}));
        
        // clear the fields
        modalNewProject.resetToDefaults();
        
        // dismiss the modal
        $('#modalNewProject').modal('hide');
    },
    'click #modalNewProject button': function(e) {
        if (e.target.id !== 'buttonNewProjectCreate' && e.target.id !== 'buttonNewProjectType') {
            modalNewProject.resetToDefaults();
        }
    }
});

var modalNewProject = {
    resetToDefaults: function() {
        Session.set(constants.sessionKeys.newProject.name, '');
        Session.set(constants.sessionKeys.newProject.error, '');
        Session.set(constants.sessionKeys.newProject.type, constants.sessionKeys.default.newProjectType);
        $('#inputNewProjectName').val('');
    }
}