Template.fileTreeContextMenu.helpers({
    contextNewFile: function() {
        return constants.application.menu.context.newFile;
    },
    contextNewFolder: function() {
        return constants.application.menu.context.newFolder;
    },
    contextRename: function() {
        return constants.application.menu.context.rename;
    },
    contextDelete: function() {
        return constants.application.menu.context.delete;
    },
    notProjectRoot: function() {
        return !isProjectObject(Session.get(constants.sessionKeys.current.contextMenuTarget));
    }
});

Template.fileTreeContextMenu.events({
    'click #contextNewFile': function() {
        modal.show('NewFile');
    },
    'click #contextNewFolder': function() {
        modal.show('NewFolder');
    },
    'click #contextRename': function() {
        modal.show('RenameFile');
    },
    'click #contextDelete': function() {
        // get the project name
        var target = Session.get(constants.sessionKeys.current.contextMenuTarget);
        var warning = target.folder ? ' This will also delete all files inside the folder.' : '';
        swal({
            title: util.format('Delete {{name}}?', {name: target.name}),
            text: warning,
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn-danger',
            confirmButtonText: util.format('Delete {{name}}.', {name: target.name}),
            closeOnConfirm: true
        }, function() {
            // if this is a folder, recursively delete all its children
            getFileChildren(target, 0).forEach(function(child) {
                Files.remove({_id: child._id});
            });
            
            // delete the current target
            Files.remove({_id: target._id});
            
            // clear the session
            Session.clear(constants.sessionKeys.current.contextMenuTarget);
        });
    }
});