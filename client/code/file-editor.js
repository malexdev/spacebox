Template.fileEditor.helpers({
    editorOptions: function() {
        return {
            lineNumbers: Session.get(constants.sessionKeys.editor.showLineNumbers),
            mode: Session.get(constants.sessionKeys.editor.codeLanguage),
            autofocus: true
        }
    }
});

Template.fileEditor.created = function() {
    startResizing();
}

Template.fileEditor.rendered = function() {
    runAfterAllRendered(function() { return Session.get(constants.sessionKeys.current.file); }, subscribeKeypressEvents);
}

Template.fileEditor.destroyed = function() {
    endResizing();
}

function subscribeKeypressEvents() {
    resize();
    $('.CodeMirror').off('keyup', triggerFileSave);
    $('.CodeMirror').on('keyup', triggerFileSave);
}

function triggerFileSave() {
    Session.set(constants.sessionKeys.current.filePendingSave, true);
    runFileSave();
}

var runFileSave = _.debounce(function() {
    console.log('Saving file');
    var text = Session.get(constants.sessionKeys.fileEditorAreaContents);
    var file = Session.get(constants.sessionKeys.current.file);
    if (text && file) {
        Session.set(constants.sessionKeys.current.filePendingSave, false);
        Files.update({_id: file._id}, {$set: {content: text}});
    }
}, 1000);

function resize() {
    $('.CodeMirror').height($(window).height() - constants.application.elementBottomOffset);
    refresh();
}

function startResizing() {
    $(window).resize(resize);
}

function endResizing() {
    $(window).off('resize', resize);
}

function refresh() {
    $('.CodeMirror').each(function(index, element){
        element.CodeMirror.refresh();
    });
}