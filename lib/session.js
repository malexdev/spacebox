if (Meteor.isClient) {
    Meteor.startup(function() {
        
    });

    /**
     * Toggle a boolean session object. Note that if the object isn't currently boolean, it will be made so...
     * @param {String} sessionKey The key for the session
     */
    toggleSessionOption = function(sessionKey) {
        Session.set(sessionKey, !Session.get(sessionKey));
    };
}
