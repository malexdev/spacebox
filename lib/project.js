Projects = new Mongo.Collection('projects');

isProjectObject = function(object) {
    if (!object) { return false; }
    return Projects.find({_id: object._id}).count() > 0;
}