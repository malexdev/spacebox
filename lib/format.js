// set up underscore templating
_.templateSettings = {
    interpolate: /\{\{(.+?)\}\}/g
};

util = {
    format: function(string, object) {
        var template = _.template(string);
        return template(object);
    }
}