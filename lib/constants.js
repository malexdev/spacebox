constants = {
    debugEnabled: false,
    keys: {
        enter: 13
    },
    application: {
        name: 'spacebox',
        elementBottomOffset: 100,
        fileTreeIndent: 10,
        menu: {
            context: {
                newFolder: 'New Folder',
                newFile: 'New File',
                rename: 'Rename',
                delete: 'Delete'
            },
            file: {
                title: 'File',
                openProject: 'Open Project',
                openFile: 'Open File',
                saveProject: 'Save Project',
                saveFile: 'Save File',
                newFolder: 'New Folder',
                newProject: 'New Project',
                newFile: 'New File'
            },
            edit: {
                title: 'Edit',
                undo: 'Undo',
                redo: 'Redo',
                cut: 'Cut',
                copy: 'Copy',
                paste: 'Paste',
                selectAll: 'Select All'
            },
            view: {
                title: 'View',
                console: 'Console',
                editor: 'Editor',
                fileBrowser: 'File Browser',
                fileHistory: 'File History'
            },
            project: {
                renameProject: 'Rename Project',
                deleteProject: 'Delete Project',
                shareProject: 'Share Project',
                closeProject: 'Close Project'
            }
        }
    },
    project: {
        types: [
            {name: 'Meteor', value: 'meteor'},
            {name: 'Node/IO.js', value: 'nodeio'},
            {name: 'Web', value: 'web'}
        ]
    },
    sessionKeys: {
        fileEditorAreaContents: 'fileEditorAreaContents',
        newProject: {
            type: 'newProjectType',
            name: 'newProjectName',
            error: 'newProjectError'
        },
        newFile: {
            error: 'newFileError'
        },
        newFolder: {
            error: 'newFolderError'
        },
        renameProject: {
            error: 'renameProjectError'
        },
        renameFile: {
            error: 'renameFileError'
        },
        openProject: {
            selectedToOpen: 'projectSelectedToOpen'
        },
        current: {
            project: 'currentProject',
            file: 'currentFile',
            filePendingSave: 'currentFilePendingSave',
            contextMenuTarget: 'currentContextMenuTarget'
        },
        view: {
            console: 'viewConsole',
            editor: 'viewEditor',
            fileBrowser: 'viewFileBrowser',
            fileHistory: 'viewFileHistory'
        },
        editor: {
            showLineNumbers: 'showLineNumbers',
            codeLanguage: 'codeLanguage'
        },
        default: {
            viewEditor: true,
            viewFileBrowser: true,
            openFiles: [],
            editingFile: false,
            viewConsole: false,
            viewFileHistory: false,
            showLineNumbers: true,
            codeLanguage: 'javascript',
            newProjectType: {name: 'Project Type', value: ''}
        }
    },
    error: {
        noProjectSelected: 'There is no project currently selected.',
        noFileSelected: 'There is no file currently selected.',
        newProject: {
            noProjectName: 'Please provide a project name.',
            noProjectType: 'Please select a project type.'
        },
        renameProject: {
            noProjectName: 'Please provide a new project name.'
        },
        renameFile: {
            noFileName: 'Please provide a new file name.'
        },
        newFile: {
            noFileName: 'Please provide a new file name.'
        },
        newFolder: {
            noFolderName: 'Please provide a new folder name.'
        }
    }
};