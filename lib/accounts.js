Meteor.startup(function() {
    if (Meteor.isClient) {
        Accounts.ui.config({
            requestPermissions: {},
            extraSignupFields: [{
                fieldName: 'first-name',
                fieldLabel: 'First name',
                inputType: 'text',
                visible: true,
                saveToProfile: true
            }, {
                fieldName: 'last-name',
                fieldLabel: 'Last name',
                inputType: 'text',
                visible: true,
                saveToProfile: true
            }]
        });
    }
});